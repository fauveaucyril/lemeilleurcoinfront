import numeral from "numeral";

export function classNames(...args: (string | boolean | undefined)[]) {
  return args.filter(Boolean).join(" ");
}

export function NumberToFormattedString(num: number): string {
  if (num < 1000) return num.toString();

  return numeral(num).format("0.0a");
}