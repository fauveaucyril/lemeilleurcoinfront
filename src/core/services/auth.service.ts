import { Jwt } from "../types/jwt";
import { User } from "../types/user";
import Service from "./service";

export class AuthService {
  static async Login(email: string, password: string): Promise<User & Jwt> {
    return Service.Request<User & Jwt>("/auth/login", {
      method: "POST",
      body: {
        email,
        password
      }
    });
  }

  static async Me(): Promise<User> {
    return Service.Request<User>("/auth/me", {
      method: "GET",
      useAuthorization: true
    });
  }
}