import { Post } from "../types/post";
import { PostCreateBody, PostListBody } from "../bodies/post.body";
import Service from "./service";

export default class TweetService {
  public static async Find(body: PostListBody): Promise<Post[]> {
    return Service.Request<Post[]>("/post", {
      useAuthorization: true,
      body,
    });
  }

  public static async Create(body: PostCreateBody): Promise<Post> {
    return Service.Request<Post>("/post", {
      method: "POST",
      useAuthorization: true,
      body,
    });
  }

  public static async Delete(postId: string): Promise<Post> {
    return Service.Request<Post>(`/post/delete/${postId}`, {
      method: "PATCH",
      useAuthorization: true,
    });
  }
}