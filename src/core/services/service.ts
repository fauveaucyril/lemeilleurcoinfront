import axios from "axios";

type HttpMethod = "GET" | "POST" | "PATCH" | "PUT" | "DELETE";
type RequestBody = { [key: string]: string | boolean | number };
type RequestHeader = { [key: string]: string };

type RequestOptions = {
  method?: HttpMethod;
  body?: RequestBody;
  headers?: RequestHeader;
  useAuthorization?: boolean;
}

export default class Service {
  public static Request<T>(endpoint: string, options?: RequestOptions): Promise<T> {
    let defaultHeaders: RequestHeader = {
      "Content-Type": "application/json",
      "Accept": "application/json",
    }

    if (options) {
      if (options.useAuthorization) {
        defaultHeaders["Authorization"] = `Bearer ${localStorage.getItem("token")}`;
      }
      
      if (options.headers) {
        defaultHeaders = { ...defaultHeaders, ...options.headers };
      }
    }

    const method = options && options.method ? options.method : "GET";

    return new Promise<T>((resolve, reject) => {
      axios.request<T>({
        url: `${process.env.REACT_APP_API_URL}${endpoint}`,
        method,
        data: options && options.body ? JSON.stringify(options.body) : undefined,
        headers: defaultHeaders,
      })
      .then(response => {
        if (response.status === 200 || response.status === 201) {
          resolve(response.data);
        }

        reject(response.statusText);
      })
      .catch(error => {
        reject(error);
      });
    });
  }
}