import { CreateUserBody } from "../bodies/create-user.body";
import { Post } from "../types/post";
import { User } from "../types/user";
import Service from "./service";

export default class UserService {
  static async Register(createUserBody: CreateUserBody): Promise<User> {
    return Service.Request<User>("/user", { method: "POST", body: createUserBody });
  }

  public static async Delete(userId: string): Promise<Post> {
    return Service.Request<Post>(`/user/delete/${userId}`, {
      method: "PATCH",
      useAuthorization: true,
    });
  }

  static async GetTimeline(): Promise<Post[]> {
    return Service.Request<Post[]>("/user/timeline", { useAuthorization: true });
  }

}