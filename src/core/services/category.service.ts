import { CreateUserBody } from "../bodies/create-user.body";
import { Category } from "../types/category";
import Service from "./service";

export default class CategoryService {
  static async GetAll(): Promise<Category> {
    return Service.Request<Category>("/category", { method: "GET"});
  }

}