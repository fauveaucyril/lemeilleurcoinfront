import { Category } from "./category";
import { User } from "./user";

export type Post = {
  _id: string;
  author: User;
  category: Category;
  title: string;
  description: string;
  images: string[];
  createdAt: string;
  updatedAt: string;
}