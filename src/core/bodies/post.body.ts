export type PostListBody = {
  _id?: string;
  author?: string;
}

export type PostCreateBody = {
  author: string;
  title: string;
  description: string;
}

export type PostPatchBody = {
  likes?: number;
}