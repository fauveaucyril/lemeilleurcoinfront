export type CreateUserBody = {
  username: string;
  email: string;
  password: string;
};
