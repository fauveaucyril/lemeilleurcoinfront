import { Fragment, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import HeaderComponent from "./components/header";
import UserService from "./core/services/user.service";
 
const Register = () => {
    const [username, setUsername] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const navigate = useNavigate();
  
    function register(): void {
      if (email.trim().length === 0 || password.trim().length === 0) return;
  
      UserService.Register({
        username,
        email,
        password
      })
        .then((value) => {
          if (!!value) {
            navigate("/");
          }
        });
    }

  return (
    <Fragment>
        <HeaderComponent></HeaderComponent>

      <main className="App-login">
        <h2 className="title">S'inscrire</h2>
        <input
          value={ username }
          onChange={ (e) => setUsername(e.target.value) }
          className=""
          type="text"
          placeholder="Nom d'utilisateur..."
        />
        <input
          value={ email }
          onChange={ (e) => setEmail(e.target.value) }
          className="" type="email" placeholder="Email..."
        />
        <input
          value={ password }
          onChange={ (e) => setPassword(e.target.value) }
          className=""
          type="password"
          placeholder="Mot de passe..."
        />
        <div className="bottom">
          <span className="register"><Link to="/login">Je suis déja inscrit</Link></span>
        </div>
        <button
          onClick={ register }
          className="submit">
            S'enregistrer
        </button>
      </main>
    </Fragment>
  );
}
 
export default Register;