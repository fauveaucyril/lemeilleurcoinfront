import { Fragment, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import HeaderComponent from "./components/header";
import { AuthService } from "./core/services/auth.service";
 
const Login = () => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const navigate = useNavigate();

  function login(): void {
    if (email.trim().length === 0 || password.trim().length === 0) return;

    AuthService.Login(email, password)
      .then((value) => {
        localStorage.setItem("token", value.token);
        if (value) {
          navigate("/");
        }
      });
  }

  return (
    <Fragment>
      <HeaderComponent></HeaderComponent>

      <main className="App-login">
        <h2 className="title">Se connecter</h2>
        <input
          value={ email }
          onChange={ (e) => setEmail(e.target.value) }
          className="mail" type="email" placeholder="Email..."
        />
        <input
          value={ password }
          onChange={ (e) => setPassword(e.target.value) }
          className="password"
          type="password"
          placeholder="Mot de passe..."
        />
        <div className="bottom">
          <span className="forgot">Mot de pase oublié ?</span>
          <span className="register"><Link to="/register">pas encore inscrit ?</Link></span>
        </div>
        <button
          onClick={ login }
          className="submit">
            Connexion
        </button>
      </main>
    </Fragment>
  );
}
 
export default Login;