import { Fragment, FunctionComponent } from "react";
import { FaUserAlt } from 'react-icons/fa';
import HeaderComponent from "./components/header";
 
type Props = {
  title: string,
  description: string,
  price: number,
}

const Annonce: FunctionComponent<Props> = (props: Props) => {
    return (
      <Fragment>
        <HeaderComponent></HeaderComponent>

        <main className="App-main post-details">
          <section className="left">
            <figure className="image">
              <img src="" alt="" />
            </figure>

            <div className="infos">
              <h3 className="title">{props.title}</h3>
              <p className="description">{props.description}</p>
              <span className="price">{props.price}€</span>
            </div>
          </section>

          <section className="right">
            <div className="account">
              <span className="icon"><FaUserAlt/></span>
              <span className="text">Utilisateur</span>
            </div>
            <a className="contact" href="">Contacter l'annonceur</a>
          </section>
        </main>
      </Fragment>
    );
}
 
export default Annonce;