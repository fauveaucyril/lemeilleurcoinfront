import { makeAutoObservable, runInAction } from "mobx";
import { PostCreateBody } from "../core/bodies/post.body";
import PostService from "../core/services/post.service";
import UserService from "../core/services/user.service";
import { Post } from "../core/types/post";

export class PostStore {
  posts: Post[] = [];

  constructor() {
    makeAutoObservable(this);
  }

  async CreatePost(createPost: PostCreateBody): Promise<Post> {
    const post = await PostService.Create(createPost);

    await this.Fetch();

    return post;
  }

  async Fetch(): Promise<void> {
    const posts = await UserService.GetTimeline();

    runInAction(() => {
      this.posts = [...posts]; 
    });
  }
}