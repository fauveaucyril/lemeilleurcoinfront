import { Fragment } from "react";
import { FaSistrix } from 'react-icons/fa';
import { Link } from "react-router-dom";

const ButtonSearchComponent = () => {
    return (
        <Fragment>
            <Link to="/" className="buttonSearch">
                <span className="search">Rechercher</span>
                <span className="icon"><FaSistrix /></span>
            </Link>
        </Fragment>
    );
}
export default ButtonSearchComponent;