import { Fragment } from "react";
import { FaUserAlt } from 'react-icons/fa';
import { FaCloudDownloadAlt } from 'react-icons/fa';
import { Link } from "react-router-dom";

const HeaderComponent = () => {
    return (
        <Fragment>
            <header className="App-header">
                <div className="inner">
                    <div className="left">
                        <Link className="logo" to="/">lemeilleurcoin</Link>
                        <Link className="postButton" to="/postAnnonce">
                            Déposer une annonce
                            <span className="icon"><FaCloudDownloadAlt /></span>
                        </Link>
                    </div>
                    <Link to="/login" className="account">
                        <span className="icon"><FaUserAlt/></span>
                        <span className="text">Mon compte</span>
                    </Link>
                </div>
            </header>
        </Fragment>
    );
}
export default HeaderComponent;