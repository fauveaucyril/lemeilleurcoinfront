import { Fragment, FunctionComponent } from "react";
import { Link } from "react-router-dom";

type Props = {
    title: string,
    description: string,
    price: number,
}

const ButtonViewComponent: FunctionComponent<Props> = (props: Props) => {
    return (
        <Fragment>
            <article className="post">
                <figure className="image"><img src="" alt="" /></figure>

                <div className="right">
                    <h3 className="title">{props.title}</h3>
                    <p className="description">{props.description}</p>
                    <span className="price">{props.price}€</span>
                    <Link className="details" to="/post">Voir l'annonce</Link>
                </div>
            </article>
        </Fragment>
    );
}
export default ButtonViewComponent;