import { Fragment, FunctionComponent, useState } from "react";
import HeaderComponent from "./components/header";
 
type Props = {

}

const DeposerAnnonce: FunctionComponent<Props> = (props: Props) => {
  const [title, setTitle] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [price, setPrice] = useState<string>("");

  function post(): void {
    
  }

  return (
    <Fragment>
      <HeaderComponent></HeaderComponent>

      <main className="App-login post-annonce">
        <h2 className="title">Déposer une annonce</h2>
        <input
          value={ title }
          onChange={ (e) => setTitle(e.target.value) }
          className=""
          type="text"
          placeholder="Titre de l'annonce"
        />
        <input
          value={ description }
          onChange={ (e) => setDescription(e.target.value) }
          className=""
          type="text"
          placeholder="Description"
        />
        <input
          value={ price }
          onChange={ (e) => setPrice(e.target.value) }
          className=""
          type="text"
          placeholder="Prix de l'annonce"
        />
        <button
          onClick={ post }
          className="submit">
            Publier l'annonce
        </button>
      </main>
    </Fragment>
  );
}
 
export default DeposerAnnonce;