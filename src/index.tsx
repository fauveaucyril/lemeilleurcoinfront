import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import {
  Routes,
  Route,
  BrowserRouter
} from "react-router-dom";
import Annonce from './Annonce';
import DeposerAnnonce from './DeposerAnnonce';
import Login from './Login';
import Register from './register';
import Account from './Account';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />} />
      <Route path="post" element={<Annonce title={"Titre de l'annonce"} description={"Ceci est la description"} price={10} />} />
      <Route path="postAnnonce" element={<DeposerAnnonce />} />
      <Route path="login" element={<Login />} />
      <Route path="register" element={<Register />} />
      <Route path="account" element={<Account />} />
    </Routes>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
