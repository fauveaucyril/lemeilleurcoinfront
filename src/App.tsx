import './App.css';

import HeaderComponent from './components/header';
import ButtonSearchComponent from './components/buttonSearch';
import ButtonViewComponent from './components/buttonView';
import CategoryService from './core/services/category.service';

function App() {
  var test = CategoryService.GetAll();
  console.log(test);
  return (
    <div className="App">
      <HeaderComponent></HeaderComponent>

      <main className="App-main">
        <section className="search-block">
          <select className="category">
          <option>Catégorie 1</option>
            <option>Catégorie 2</option>
          </select>
          <input className="search" placeholder="Votre recherche ici"></input>
          <ButtonSearchComponent></ButtonSearchComponent>
        </section>

        <section className="list-posts">
          <h2>Résultats de la recherche</h2>
          <ButtonViewComponent title="Titre de l'annonce" description="Ceci est la description" price={20} />
          <ButtonViewComponent title="Titre de l'annonce 2" description="Ceci est la description" price={50} />
          <ButtonViewComponent title="Titre de l'annonce 3" description="Ceci est la description" price={10} />
        </section>
      </main>
    </div>
  );
}

export default App;
